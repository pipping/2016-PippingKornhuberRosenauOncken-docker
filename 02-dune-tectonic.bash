#!/usr/bin/env bash

set -e

tag=2016-PippingKornhuberRosenauOncken
docker build \
    -t pipping/dune-tectonic:${tag} 02-dune-tectonic

tmpdir=`mktemp -d`
docker run --rm -v $tmpdir:/bridge pipping/dune-tectonic:${tag} \
    sh -c 'tar -C /dune-build/dune-tectonic/src -cf /bridge/dune-tectonic.tar \
               one-body-problem-2D \
               one-body-problem-3D \
               uniform-grid-writer-2D \
               uniform-grid-writer-3D && \
           tar -C /dune-sources/dune-tectonic/src -rf /bridge/dune-tectonic.tar \
               one-body-problem.cfg \
               one-body-problem-2D.cfg \
               one-body-problem-3D.cfg && \
           tar -C /usr/lib/x86_64-linux-gnu -h -rf /bridge/dune-tectonic.tar \
               libhdf5_serial.so.100 \
               libhdf5_serial_hl.so.100 \
               libstdc++.so.6 && \
           tar -C /lib/x86_64-linux-gnu -h -rf /bridge/dune-tectonic.tar \
               ld-linux-x86-64.so.2 \
               libaec.so.0 \
               libc.so.6 \
               libdl.so.2 \
               libgcc_s.so.1 \
               libm.so.6 \
               libpthread.so.0 \
               libsz.so.2 \
               libz.so.1'
lzip -9 $tmpdir/dune-tectonic.tar
mv $tmpdir/dune-tectonic.tar.lz .
rmdir $tmpdir
