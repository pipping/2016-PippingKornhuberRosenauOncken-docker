FROM debian:9
LABEL description="R environment to generate data that LaTeX can read"
MAINTAINER elias.pipping@fu-berlin.de

ENV ARTDIR=/article
ENV DATADIR=/data
ENV OUTDIR=/output

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        make g++ libhdf5-dev r-base-core wget && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir -p ${ARTDIR}
WORKDIR ${ARTDIR}

ENV RCPP_VERSION=0.12.12
RUN wget -O Rcpp_${RCPP_VERSION}.tar.gz \
        https://github.com/cran/Rcpp/archive/${RCPP_VERSION}.tar.gz && \
    R CMD INSTALL Rcpp_${RCPP_VERSION}.tar.gz && \
    rm -f Rcpp_${RCPP_VERSION}.tar.gz

ENV H5_VERSION=0.9.9
RUN wget -O h5_${H5_VERSION}.tar.gz \
        https://github.com/mannau/h5/archive/release-cran-${H5_VERSION}.tar.gz && \
    R CMD INSTALL h5_${H5_VERSION}.tar.gz && \
    rm -f h5_${H5_VERSION}.tar.gz

ENV INI_VERSION=0.2
RUN wget -O ini_${INI_VERSION}.tar.gz \
        https://github.com/cran/ini/archive/${INI_VERSION}.tar.gz && \
    R CMD INSTALL ini_${INI_VERSION}.tar.gz && \
    rm -f ini_${INI_VERSION}.tar.gz

ENV DUNE_TECTONIC_BRANCH=2016-PippingKornhuberRosenauOncken

## Get last commit to invalidate cache if necessary
ADD https://git.imp.fu-berlin.de/api/v4/projects/pipping%2F${DUNE_TECTONIC_BRANCH}-plots/repository/branches/master \
        dune-${DUNE_TECTONIC_BRANCH}-plots.head
RUN wget -O ${DUNE_TECTONIC_BRANCH}-plots.tar \
        https://git.imp.fu-berlin.de/pipping/${DUNE_TECTONIC_BRANCH}-plots/repository/archive.tar?ref=master && \
    tar xf ${DUNE_TECTONIC_BRANCH}-plots.tar && \
    rm -f ${DUNE_TECTONIC_BRANCH}-plots.tar && \
    mv ${DUNE_TECTONIC_BRANCH}-plots-master-* \
        ${DUNE_TECTONIC_BRANCH}-plots

WORKDIR ${DUNE_TECTONIC_BRANCH}-plots
RUN echo "[directories]"                 > config.ini && \
    echo "simulation = ${DATADIR}"      >> config.ini && \
    echo "experiment = ${DATADIR}"      >> config.ini && \
    echo "output = ${OUTDIR}/generated" >> config.ini
CMD tools/generate-2d.bash && \
    tools/generate-3d.bash && \
    tools/generate-others.bash
