#!/usr/bin/env bash

set -e

docker build \
    -t pipping/latex-figures 05-latex-figures

tag=2016-PippingKornhuberRosenauOncken

(
    cd 05-latex-figures-data
    rm -rf generated
    tar xf generated.tar.lz
)

tmpdir=`mktemp -d`
docker run --rm -v ${PWD}/05-latex-figures-data:/data -v ${tmpdir}:/output pipping/latex-figures
(
    cd ${tmpdir}
    tar cf figures.tar *.png
    rm *.png
    lzip -9 figures.tar
)
mv ${tmpdir}/figures.tar.lz .
rmdir ${tmpdir}

(
    cd 05-latex-figures-data
    rm -rf generated
)
