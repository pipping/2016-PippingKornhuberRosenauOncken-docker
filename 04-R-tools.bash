#!/usr/bin/env bash

set -e

docker build \
    -t pipping/r-tools 04-R-tools

tag=2016-PippingKornhuberRosenauOncken
out=05-latex-figures-data

tmpdir=`mktemp -d`
docker run --rm -v $PWD/04-R-tools-data:/data -v ${tmpdir}:/output pipping/r-tools
(
    cd ${tmpdir}
    tar cf generated.tar generated
    rm -rf generated
    lzip -9 generated.tar
)
mv ${tmpdir}/generated.tar.lz ${out}/
rmdir ${tmpdir}
