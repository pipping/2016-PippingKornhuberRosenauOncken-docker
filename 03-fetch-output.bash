#!/usr/bin/env bash

set -e

tag=2016-PippingKornhuberRosenauOncken
out=04-R-tools-data
baseurl=https://ftp.math.fu-berlin.de/pub/pipping/${tag}
dirs=(
    2d-lab-fpi-tolerance/rfpitol={{1,2,3,5}{,0{,0{,0{,0}}}},100000}e-7
    3d-lab/rtol=1e-5_diam=1e-2
)
for dir in ${dirs[@]}; do
    mkdir -p ${out}/${dir}

    sum=$(wget ${baseurl}/${dir}/sha256sum -O - 2>/dev/null)
    if echo ${sum%compressed.h5} ${out}/${dir}/output.h5 | sha256sum --check 2>/dev/null; then
        continue
    else
        echo "${dir}/compressed.h5 corrupt or missing. (Re-)downloading."
    fi

    wget -O ${out}/${dir}/output.h5 ${baseurl}/${dir}/compressed.h5 2>/dev/null
    if ! echo ${sum%compressed.h5} ${out}/${dir}/output.h5 | sha256sum --check 2>/dev/null; then
        echo "WARNING: ${dir}/compressed.h5 corrupt."
    fi
done
# No HTTPS, no checksums
if ! [[ -f ${out}/B_Scale-model-earthquake-data.zip ]]; then
    wget -O ${out}/B_Scale-model-earthquake-data.zip \
        http://escidoc.gfz-potsdam.de/ir/item/escidoc:1873905/components/component/escidoc:2201891/content 2>/dev/null
fi
